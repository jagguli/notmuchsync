from unittest import TestCase
from notmuchsync import NotmuchCal, GoogleCal, CalEventMapper, OrgCal
import icalendar
from random import randint
import copy

class BaseTest(TestCase):
    event = {
        'summary': 'Google I/O 2015 - test event %s' %  randint(0, 9999),
        'location': '800 Howard St., San Francisco, CA 94103',
        'description': 'A chance to hear more about Google\'s developer products.',
        'start': {
            'dateTime': '2015-08-28T09:00:00-07:00',
            'timeZone': 'Australia/Sydney',
        },
        'end': {
            'dateTime': '2015-08-28T17:00:00-07:00',
            'timeZone': 'Australia/Sydney',
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
            {'email': 'stevenjose@gmail.com'},
            {'email': 'steven@stevenjoseph.in'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
        'iCalUID': 'originalUID'
    }

class NotmuchCalTest(TestCase):
    def test_get_all_invitest(self):
        nm = NotmuchCal()
        print(nm.get_all_invites())

class OrgCalTest(BaseTest):
    def test_add(self):
        org = OrgCal()
        event = copy.deepcopy(self.event)
        org.add(event)
    def test_get(self):
        org = OrgCal()
        event = copy.deepcopy(self.event)
        org.get(event['iCalUID'])

class GoogleCalTest(TestCase):
    calname = 'notmuch'

    def test_get_calendar(self):
        gc = GoogleCal()
        calendar = gc.get_calendar(self.calname)
        assert calendar['summary'] == self.calname, calendar

    def test_get_events(self):
        gc = GoogleCal()
        assert len(gc.get_events(self.calname)) > 0

    def test_add_event(self):
        event = {
            'summary': 'Google I/O 2015 - test event',
            'location': '800 Howard St., San Francisco, CA 94103',
            'description': 'A chance to hear more about Google\'s developer products.',
            'start': {
                'dateTime': '2015-08-28T09:00:00-07:00',
                'timeZone': 'Australia/Sydney',
            },
            'end': {
                'dateTime': '2015-08-28T17:00:00-07:00',
                'timeZone': 'Australia/Sydney',
            },
            'recurrence': [
                'RRULE:FREQ=DAILY;COUNT=2'
            ],
            'attendees': [
                {'email': 'stevenjose@gmail.com'},
                {'email': 'steven@stevenjoseph.in'},
            ],
            'reminders': {
                'useDefault': False,
                'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10},
                ],
            },
            'iCalUID': 'originalUID'
        }
        gc = GoogleCal()
        print gc.add_event(self.calname, event)

    def test_import_event(self):
        from random import randint
        event = {
            'summary': 'Google I/O 2015 - test event %s' % randint(0, 9999),
            'location': '800 Howard St., San Francisco, CA 94103',
            'description': 'A chance to hear more about Google\'s developer products.',
            'start': {
                'dateTime': '2015-08-28T09:00:00-07:00',
                'timeZone': 'Australia/Sydney',
            },
            'end': {
                'dateTime': '2015-08-28T17:00:00-07:00',
                'timeZone': 'Australia/Sydney',
            },
            'recurrence': [
                'RRULE:FREQ=DAILY;COUNT=2'
            ],
            'attendees': [
                {'email': 'stevenjose@gmail.com'},
                {'email': 'steven@stevenjoseph.in'},
            ],
            'reminders': {
                'useDefault': False,
                'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10},
                ],
            },
            'iCalUID': 'originalUID'
        }
        gc = GoogleCal()
        print gc.import_event(self.calname, event)


class CalEventMapperTest(TestCase):
    event = {
        'summary': 'Google I/O 2015 - test event %s' %  randint(0, 9999),
        'location': '800 Howard St., San Francisco, CA 94103',
        'description': 'A chance to hear more about Google\'s developer products.',
        'start': {
            'dateTime': '2015-08-28T09:00:00-07:00',
            'timeZone': 'Australia/Sydney',
        },
        'end': {
            'dateTime': '2015-08-28T17:00:00-07:00',
            'timeZone': 'Australia/Sydney',
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
            {'email': 'stevenjose@gmail.com'},
            {'email': 'steven@stevenjoseph.in'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
        'iCalUID': 'originalUID'
    }

    def test_get_gcal_event(self):
        event = self.event
        ical = open("test.ical").read().format(
            summary=event['summary'],
        )
        nm1 = icalendar.Event.from_ical(ical)
        mapper = CalEventMapper()

        res = mapper.get_gcal_event(nm1)
        assert res['summary'] == event['summary'], (res['summary'], event['summary'])


    def test_get_org_event(self):
        event = self.event
        ical = open("test.ical").read().format(
            summary=event['summary'],
        )
        nm1 = icalendar.Event.from_ical(ical)
        mapper = CalEventMapper()

        res = mapper.get_org_event(nm1)
        assert res['summary'] == event['summary'], (res['summary'], event['summary'])








