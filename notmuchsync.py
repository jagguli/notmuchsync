#!/usr/bin/env python2
import notmuch
import httplib2
import os

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools

import datetime
from icalendar import Calendar, Event
from datetime import datetime
from PyOrgMode import PyOrgMode
import copy

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Quickstart'

notmuch_gcal_map = {
    'summary': 'summary',
}
    

class NotmuchCal():
    def __init__(self):
        self.db = notmuch.Database()

    def get_all_invites(self, cancelled=False):
        invites = []
        query = self.db.create_query('tag:calendar')
        for message in query.search_messages(): # doctest:+ELLIPSIS
            parts = message.get_message_parts()
            for i, part in enumerate(parts):
                if part.get_content_type() != 'text/calendar':
                    continue
                if not cancelled and part.get_param('method') == 'CANCEL':
                    print("CANCEL: %s" % message.get_header('Subject'))
                    continue
                    #print(part.get_payload(decode=True))
                else:
                    print("SYNC: %s" % message.get_header('Subject'))
                invites.append(part)
        return invites


class GoogleCal():
    def __init__(self):
        self.credentials = self.get_credentials()
        self.http = self.credentials.authorize(httplib2.Http())
        self.service = discovery.build('calendar', 'v3', http=self.http)

    def insert(self, part):
        gcal = Calendar.from_ical(part.get_payload(decode=True))
        results = []
        for component in gcal.walk():
            if component.name == "VEVENT":
                results.append(component)
                print str(component.get('summary'))
                print str(component.get('dtstart'))
                print str(component.get('dtend'))
                print str(component.get('dtstamp'))
        return results

    def get_credentials(self, flags=None):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        from argparse import Namespace
        flags = Namespace()
        flags.noauth_local_webserver = True
        flags.logging_level = 'DEBUG'
        
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                    'calendar.json')

        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatability with Python 2.6
                credentials = tools.run(flow, store)
            print 'Storing credentials to ' + credential_path
        return credentials

    def list_calendars(self):
        page_token = None
        while True:
            calendar_list = self.service.calendarList().list(
                pageToken=page_token).execute()
            for calendar_list_entry in calendar_list['items']:
                print calendar_list_entry['summary']
            page_token = calendar_list.get('nextPageToken')
            if not page_token:
                break

    def get_calendar(self, calendarname):
        page_token = None
        while True:
            calendar_list = self.service.calendarList().list(
                pageToken=page_token).execute()
            for calendar_list_entry in calendar_list['items']:
                if calendarname == str(calendar_list_entry['summary']):
                    print(calendar_list_entry)
                    return calendar_list_entry
            page_token = calendar_list.get('nextPageToken')
            if not page_token:
                break

    def get_events(self, calendar='primary'):
        """Shows basic usage of the Google Calendar API.

        Creates a Google Calendar API service object and outputs a list of the next
        10 events on the user's calendar.
        """
        now = datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        print 'Getting the upcoming 10 events'
        calendarId = self.get_calendar(calendar)['id']

        eventsResult = self.service.events().list(
            calendarId=calendarId,
            timeMin=now, maxResults=10, singleEvents=True,
            orderBy='startTime').execute()
        events = eventsResult.get('items', [])

        if not events:
            print 'No upcoming events found.'
        for event in events:
            start = event['start'].get('dateTime', event['start'].get('date'))
            print start, event['summary']
        return events

    def add_event(self, calendar, event):
        # https://developers.google.com/google-apps/calendar/v3/reference/events/import
        calendarid = self.get_calendar(calendar)['id']
        event = self.service.events().insert(
            calendarId=calendarid, body=event).execute()
        print 'Event created: %s' % (event.get('htmlLink'))
    def import_event(self, calendar, event):
        # https://developers.google.com/google-apps/calendar/v3/reference/events/import
        calendarid = self.get_calendar(calendar)['id']
        event = self.service.events().import_(
            calendarId=calendarid, body=event).execute()
        print 'Event imported: %s' % (event.get('htmlLink'))


class OrgCal():
    def __init__(self, orgfile="agenda.org"):
        self.orgfile = orgfile

    def list(self):
        org = PyOrgMode.OrgDataStructure()
        org.load_from_file(self.orgfile)

    def get_scheduled_elements(self, element, data=None):
        """
        Grab the data from all scheduled elements for all the tree defined by 'element' recursively.
        Returns all the elements as an array.
        """
        if hasattr(element, "content"):
            for child in element.content:
                if hasattr(child, "TYPE"):
                    if child.TYPE == "SCHEDULE_ELEMENT" \
                      and child.type == PyOrgMode.OrgSchedule.Element.SCHEDULED:
                        # This element is scheduled, we are creating a copy of it
                        data.append(copy.deepcopy(child.parent))
                self.get_scheduled_elements(child, data)
        return data
    def find_property(self, element, key='', value=None, data=None):
        if hasattr(element, "content"):
            for child in element.content:
                if isinstance(child, PyOrgMode.OrgDrawer.Property) \
                  and getattr(child, 'name', '') == key:
                    data.append(copy.deepcopy(child.parent))
                self.find_property(child, key=key, data=data)
        return data

    def get(self, event):
        base = PyOrgMode.OrgDataStructure()
        base.load_from_file(self.orgfile)
        sche = self.get_scheduled_elements(base.root, [])
        print [s.heading for s in sche]

    def add(self, event):
        base = PyOrgMode.OrgDataStructure()
        try:
            base.load_from_file(self.orgfile)
        except Exception as e:
            print e
        found = self.find_property(base.root, key='UID', data=[])
        new_todo = found[0] if found else PyOrgMode.OrgNode.Element()
        new_todo.heading = event['summary']
        new_todo.tags=["things", "important", "calendar"]
        new_todo.level = 1
        new_todo.todo = "TODO"
        if not found:
            import sj; sj.debug() ######## FIXME:REMOVE ME steven.joseph ################
            _sched = PyOrgMode.OrgSchedule()
            _sched._append(new_todo, _sched.Element(scheduled="<2015-08-01 Sat 12:00-13:00>"))
            _sched._append(new_todo, _sched.Element(deadline="<2015-08-01 Sat 12:00-13:00>"))
            _props = PyOrgMode.OrgDrawer.Element("PROPERTIES")
            # Add a properties drawer
            _props.append(PyOrgMode.OrgDrawer.Property("UID", event['iCalUID']))
            _props.append(PyOrgMode.OrgDrawer.Property("LASTSYNC", "bob, sally"))
            # Append the properties to the new todo item
            new_todo.append_clean(_props)
            base.root.append_clean(new_todo)
        base.save_to_file(self.orgfile)
    

class CalEventMapper():
    """This mapper maps the native event data structure to other event structures
    """
    mapping = {
        'summary': 'summary',
        'iCalUID': 'UID',
    }
    def get_event(self, nmcal):
        event = {}
        for sub in nmcal.walk(name='VEVENT'):
            for key, value in self.mapping.items():
                event[key] = str(sub.get(value))
        return event

    def get_gcal_event(self, nmcal):
        return self.get_event(nmcal)

    def get_org_event(self, nmcal):
        org = OrgCal()
        event = self.get_event(nmcal)
        return org.add(event)


if __name__ == '__main__':
    print(google_events())
